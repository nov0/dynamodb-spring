Use SWAGGER 2 documentation for all endpoints on:
http://localhost:8080/swagger-ui.html <br/>
on server <br/>
http://ec2-18-184-32-156.eu-central-1.compute.amazonaws.com:8081/swagger-ui.html

Example endpoint:
Users:
http://ec2-18-184-32-156.eu-central-1.compute.amazonaws.com:8081/user/getAll

Movies from 1980 year:
http://ec2-18-184-32-156.eu-central-1.compute.amazonaws.com:8081/movie/getByYear?year=1980