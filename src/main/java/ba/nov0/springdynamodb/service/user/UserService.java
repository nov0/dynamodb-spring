package ba.nov0.springdynamodb.service.user;

import ba.nov0.springdynamodb.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    void createTable() throws Exception;

    void delete(User user);

    User edit(User user);

    void delete(String id);

    User save(User user);

    Iterable<User> getAll();

    Optional<List<User>> getAllByName(String email);

    void removeTable() throws Exception;

    Optional<User> getById(String id);
}
