package ba.nov0.springdynamodb.service.user;

import ba.nov0.springdynamodb.domain.User;
import ba.nov0.springdynamodb.repositories.user.UserRepository;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private static String USER_TABLE_NAME = "Users";
    private static String ATTRIBUTE_NAME_ID = "id";


    private final UserRepository userRepository;
    private final DynamoDB dynamoDB;

    public UserServiceImpl(UserRepository userRepository, DynamoDB dynamoDB) {
        this.userRepository = userRepository;
        this.dynamoDB = dynamoDB;
    }

    @Override
    public void createTable() throws Exception {
        logger.info("Creating table: " + USER_TABLE_NAME);
        Table table = dynamoDB.createTable(
                USER_TABLE_NAME,
                Collections.singletonList(new KeySchemaElement(ATTRIBUTE_NAME_ID, KeyType.HASH)),
                Collections.singletonList(new AttributeDefinition(ATTRIBUTE_NAME_ID, ScalarAttributeType.S)),
                new ProvisionedThroughput(10L, 10L)
        );
        TableDescription tableDescription = table.waitForActive();
        logger.info("Table successfully created\n" + tableDescription.toString());
    }

    @Override
    public void removeTable() throws Exception {
        Table table = dynamoDB.getTable(USER_TABLE_NAME);
        DeleteTableResult deleteTable = table.delete();
        table.waitForDelete();
        logger.info("table delete " + deleteTable.toString());
    }

    @Override
    public Optional<User> getById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User edit(User user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(String id) {
        userRepository.deleteById(id);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public Iterable<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<List<User>> getAllByName(String name) {
        return userRepository.findAllByNameContaining(name);
    }
}
