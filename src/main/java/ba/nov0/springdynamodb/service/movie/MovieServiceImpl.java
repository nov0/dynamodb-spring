package ba.nov0.springdynamodb.service.movie;

import ba.nov0.springdynamodb.domain.Movie;
import ba.nov0.springdynamodb.repositories.movie.MovieRepository;
import ba.nov0.springdynamodb.service.user.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final MovieRepository movieRepository;

    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public List<Movie> getByYear(int year) {
        return movieRepository.getByYear(year);
    }

    @Override
    public Movie save(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public void remove(int year, String title) {
        movieRepository.remove(year, title);
    }

}
