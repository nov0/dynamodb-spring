package ba.nov0.springdynamodb.service.movie;

import ba.nov0.springdynamodb.domain.Movie;

import java.util.List;

public interface MovieService {

    List<Movie> getByYear(int year);

    Movie save(Movie movie);

    void remove(int year, String title);
}
