package ba.nov0.springdynamodb.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@DynamoDBDocument
public class Info {

    private List<String> directors;
    @JsonProperty("release_date")
    @DynamoDBAttribute(attributeName = "release_date")
    private String releaseDate;
    private Double rating;
    private List<String> genres;
    @JsonProperty("image_url")
    @DynamoDBAttribute(attributeName = "image_url")
    private String imageUrl;
    private String plot;
    private Integer rank;
    @JsonProperty("running_time_secs")
    @DynamoDBAttribute(attributeName = "running_time_secs")
    private Integer runningTimeSecs;
    private List<String> actors;


    public List<String> getDirectors() {
        return directors;
    }

    public void setDirectors(List<String> directors) {
        this.directors = directors;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getRunningTimeSecs() {
        return runningTimeSecs;
    }

    public void setRunningTimeSecs(Integer runningTimeSecs) {
        this.runningTimeSecs = runningTimeSecs;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

}
