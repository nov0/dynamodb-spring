package ba.nov0.springdynamodb.repositories.movie;

import ba.nov0.springdynamodb.domain.Movie;

import java.util.List;

public interface MovieRepository {

    List<Movie> getByYear(int year);

    Movie save(Movie save);

    void remove(int year, String title);
}
