package ba.nov0.springdynamodb.repositories.movie;

import ba.nov0.springdynamodb.domain.Movie;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MovieRepositoryImpl implements MovieRepository {

    private static final Logger logger = LoggerFactory.getLogger(MovieRepositoryImpl.class);
    private static final String MOVIE_TABLE_NAME = "Movies";

    private final DynamoDB dynamoDB;
    private final DynamoDBMapper dynamoDbMapper;

    public MovieRepositoryImpl(DynamoDB dynamoDB, DynamoDBMapper dynamoDbMapper) {
        this.dynamoDB = dynamoDB;
        this.dynamoDbMapper = dynamoDbMapper;
    }

    private Table getTable() {
        return dynamoDB.getTable(MOVIE_TABLE_NAME);
    }

    @Override
    public List<Movie> getByYear(int year) {
        Map<String, String> expressionAttributeNames = new HashMap<>();
        expressionAttributeNames.put("#year", "year");

        Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();
        expressionAttributeValues.put(":year", new AttributeValue().withN("" + year));

        DynamoDBQueryExpression<Movie> movieDynamoDBQueryExpression = new DynamoDBQueryExpression<Movie>()
                .withKeyConditionExpression("#year = :year")
                .withExpressionAttributeNames(expressionAttributeNames)
                .withExpressionAttributeValues(expressionAttributeValues);

        return dynamoDbMapper.query(Movie.class, movieDynamoDBQueryExpression);
    }

    @Override
    public Movie save(Movie movie) {
        Table movieTable = getTable();
        try {
            Item movieItem = new Item()
                    .withPrimaryKey(
                            "year", movie.getYear(),
                            "title", movie.getTitle()
                    )
                    .withJSON("info", new ObjectMapper().writeValueAsString(movie.getInfo()));
            PutItemOutcome newMovieItemOutcome = movieTable.putItem(movieItem);
            logger.info("Movie item successfully crated" + newMovieItemOutcome.getPutItemResult());
        } catch (JsonProcessingException e) {
            logger.error("Error while saving new movie: " + e.getLocalizedMessage());
        }
        return movie;
    }

    @Override
    public void remove(int year, String title) {
        Table table = getTable();
        PrimaryKey primaryKey = new PrimaryKey("year", year, "title", title);
        table.deleteItem(primaryKey);
        logger.info("Deleted item with key" + primaryKey);
    }

}
