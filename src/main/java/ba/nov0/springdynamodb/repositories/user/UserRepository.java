package ba.nov0.springdynamodb.repositories.user;

import ba.nov0.springdynamodb.domain.User;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

@EnableScan
public interface UserRepository extends CrudRepository<User, String> {

    Optional<List<User>> findAllByNameContaining(String name);

}