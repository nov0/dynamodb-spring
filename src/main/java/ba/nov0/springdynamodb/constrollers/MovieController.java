package ba.nov0.springdynamodb.constrollers;

import ba.nov0.springdynamodb.domain.Movie;
import ba.nov0.springdynamodb.service.movie.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("movie")
public class MovieController {

    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/getByYear")
    public ResponseEntity getAll(@RequestParam("year") int year) {
        List<Movie> moviesByYear = movieService.getByYear(year);
        Map<String, Object> response = new HashMap<>();
        response.put("count", moviesByYear.size());
        response.put("data", moviesByYear);
        return ResponseEntity.ok(response);
    }

    @PutMapping(value = "/", consumes = "application/json")
    public ResponseEntity save(@RequestBody Movie movie) {
        Movie savedMovie = movieService.save(movie);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedMovie);
    }

    @DeleteMapping(value = "/{year}/{title}")
    public ResponseEntity delete(@PathVariable("year") int year, @PathVariable("title") String title) {
        movieService.remove(year, title);
        return ResponseEntity.ok().build();
    }
}
