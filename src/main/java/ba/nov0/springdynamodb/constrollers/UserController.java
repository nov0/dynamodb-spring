package ba.nov0.springdynamodb.constrollers;

import ba.nov0.springdynamodb.domain.User;
import ba.nov0.springdynamodb.service.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") String id) {
        Optional<User> user = userService.getById(id);
        return ResponseEntity.ok(user);
    }

    @GetMapping("/getAll")
    public ResponseEntity getAll() {
        return ResponseEntity.ok(userService.getAll());
    }

    @GetMapping("/getAllByName")
    public ResponseEntity getAllByName(@RequestParam("name") String name) {
        Optional<List<User>> allByName = userService.getAllByName(name);
        return ResponseEntity.ok(allByName);
    }

    @PutMapping(value = "/", consumes = "application/json")
    public ResponseEntity create(@RequestBody User user) {
        User savedUser = userService.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedUser);
    }

    @PostMapping(value = "/", consumes = "application/json")
    public ResponseEntity edit(@RequestBody User user) {
        if (user != null && !StringUtils.isEmpty(user.getId())) {
            User updatedUser = userService.save(user);
            return ResponseEntity.ok(updatedUser);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") String id) {
        userService.delete(id);
        return ResponseEntity.ok().build();
    }

    ///// table manipulation

    @PutMapping("/table")
    public ResponseEntity createTable() {
        Map<String, String> resp = new HashMap<>();
        try {
            userService.createTable();
            resp.put("info", "User table successfully created");
            resp.put("status", "OK");
            return ResponseEntity.status(HttpStatus.CREATED).body(resp);
        } catch (Exception e) {
            resp.put("info", "error while creating table");
            resp.put("status", "error");
            e.printStackTrace();
            return ResponseEntity.badRequest().body(resp);
        }
    }

    @DeleteMapping("/table")
    public ResponseEntity removeTable() {
        Map<String, String> resp = new HashMap<>();
        try {
            userService.removeTable();
            resp.put("status", "OK");
            resp.put("info", "User table successfully deleted");
            return ResponseEntity.ok(resp);
        } catch (Exception e) {
            resp.put("info", "error while deleting table");
            resp.put("status", "error");
            return ResponseEntity.badRequest().body(resp);
        }
    }
}
