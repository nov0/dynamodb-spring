package ba.nov0.springdynamodb.constrollers;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HomeController {

    @GetMapping(value = "/")
    public ResponseEntity home() {
        Map<String, String> resp = new HashMap<>();
        resp.put("endpoint", "home");
        resp.put("status", "OK");
        return ResponseEntity.ok(resp);
    }
}
